export type FighterDetails = {
    _id: string;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
};

export type Fighter = {
    _id: string;
    name: string;
    source: string;
}
