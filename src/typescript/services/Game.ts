import { FighterDetails } from '../types/index';

export class Game {
    isOver: boolean;
    winner: FighterDetails | null;
    constructor(isOver = false, winner: FighterDetails | null) {
        this.isOver = isOver;
        this.winner = winner;
    }
    updateGame(winner: FighterDetails) {
        this.isOver = true;
        this.winner = winner;
    }
}