import { FighterDetails } from '../types/index';
import { getDamage, getCriticalDamage } from "../components/fight";
import { Game } from './Game';

export class Player {
    readonly fighter: FighterDetails;
    healthIndicator: HTMLElement;
    game: Game;
    blockOn: boolean
    health: number;
    criticalAvaible: boolean;
    constructor(fighter: FighterDetails, healthIndicator: HTMLElement, game: Game) {
        this.fighter = fighter;
        this.healthIndicator = healthIndicator;
        this.game = game;
        this.blockOn = false;
        this.health = fighter.health
        this.criticalAvaible = true;
    }
    
    public hit(opponent: Player) {
        if (this.blockOn) return;
        const damage = getDamage(this.fighter, opponent.fighter);
        if (!opponent.blockOn) {
            opponent.health -= damage;
            opponent.setIndicatorWidth(opponent.health);
        }
        if (opponent.health <= 0) {
            this.game.updateGame(this.fighter);
        }
    }
    public criticalHit(opponent: Player) {
        if (this.blockOn || !this.criticalAvaible) return;
        const damage = getCriticalDamage(this.fighter);
        opponent.health -= damage;
        opponent.setIndicatorWidth(opponent.health);
        if (opponent.health <= 0) {
            this.game.updateGame(this.fighter);
        }
        this.criticalAvaible = false;
        setTimeout(() => this.criticalAvaible = true, 10000);
    }
    public setIndicatorWidth(health: number) {
        const healthPercentage = 100 * health / this.fighter.health;
        this.healthIndicator.style.width = `${healthPercentage}%`;
    }
}