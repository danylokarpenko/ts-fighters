import { controls as controlsType } from '../../constants/controls';
import { Player } from './Player';
export class Input {
    constructor(controls: typeof controlsType, playerOne: Player, playerTwo: Player) {
        document.addEventListener('keydown', (event) => {
            const { code } = event;
            switch (code) {
                case controls.PlayerOneAttack:
                    playerOne.hit(playerTwo);
                    break;
                case controls.PlayerTwoAttack:
                    playerTwo.hit(playerOne);
                    break;
                case controls.PlayerOneBlock:
                    playerOne.blockOn = true;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = true;
                    break;
            }
        })
        document.addEventListener('keyup', (event) => {
            const { code } = event;
            switch (code) {
                case controls.PlayerOneBlock:
                    playerOne.blockOn = false;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = false;
                    break;
            }
        })
        this.runOnKeyCombination(() => {
            playerOne.criticalHit(playerTwo);
        }, controls.PlayerOneCriticalHitCombination);

        this.runOnKeyCombination(() => {
            playerTwo.criticalHit(playerOne);
        }, controls.PlayerTwoCriticalHitCombination);
    }

    runOnKeyCombination(func: ()=> void, codes: string[]) {
        const pressed: Set<string> = new Set();
        document.addEventListener('keydown', function (event) {
            pressed.add(event.code);
            for (const code of codes) {
                if (!pressed.has(code)) {
                    return;
                }
            }
            pressed.clear();
            func();
        });
        document.addEventListener('keyup', function (event) {
            pressed.delete(event.code);
        });
    }
} 