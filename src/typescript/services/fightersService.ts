import { FighterDetails, Fighter } from '../types/index'
import { callApi } from '../helpers/apiHelper';

class FighterService {
  public async getFighters() {
    const endpoint = 'fighters.json';
    const apiResult: Fighter[] = await callApi(endpoint, 'GET');

    return apiResult;
  }
  public async getFighterDetails(id: string): Promise<FighterDetails> {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult: FighterDetails = await callApi(endpoint, 'GET');

    return apiResult;
  }
}

export const fighterService = new FighterService();
