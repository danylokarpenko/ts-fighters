import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { FighterDetails } from '../types/index';
const versusImgSrc = '../../../resources/versus.png';

export function createFightersSelector() {
  const selectedFighters: FighterDetails[] = [];

  return async (event: Event, fighterId: string): Promise<void> => {
    const fighter: FighterDetails = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;

    if (!playerOne && fighter) {
      selectedFighters[0] = fighter;
    }
    if (playerOne && fighter && !playerTwo) {
      selectedFighters[1] = fighter;
    }

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<FighterDetails> {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId)
  }
  const figtherInfo = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(figtherInfo._id, figtherInfo);
  return figtherInfo;
}

function renderSelectedFighters(selectedFighters: FighterDetails[]): void {
  const fightersPreview: HTMLDivElement | null = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left');
  const secondPreview = createFighterPreview(playerTwo, 'right');
  const versusBlock = createVersusBlock(selectedFighters);
  
  if (fightersPreview) {
    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  }
}

function createVersusBlock(selectedFighters: FighterDetails[]): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImgSrc },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: FighterDetails[]): void {
  renderArena(selectedFighters);
}
