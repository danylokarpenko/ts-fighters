import { createElement } from '../../helpers/domHelper';

export function showModal(modalData: { title: string, bodyElement: HTMLElement, onClose?: () => void }) {
  const {
    title,
    bodyElement, 
    onClose
  } = modalData;

  const root = getModalContainer();
  const modal = createModal({ title, bodyElement, onClose });
  
  root!.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(modalData: { title: string, bodyElement: HTMLElement, onClose?: () => void }) {
  const {
    title,
    bodyElement, 
    onClose
  } = modalData;

  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void = () => ({})) {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal!.remove();
}
