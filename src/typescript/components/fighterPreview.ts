import { FighterDetails, Fighter } from '../types/index'
import { createElement } from '../helpers/domHelper';
// ts done 08 22:41
export function createFighterPreview(fighter: FighterDetails, position: 'right' | 'left'): HTMLElement {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    if (fighter) {
        const fighterIm = createFighterImage(fighter);
        const fighterName = createFighterName(fighter);
        const fighterHealth = createFighterHealth(fighter);
        const fighterDefense = createFighterDefense(fighter);
        const fighterAttack = createFighterAttack(fighter);

        fighterElement.append(fighterIm, fighterName, fighterHealth, fighterDefense, fighterAttack)
    }

    return fighterElement;
}

function createFighterName(fighter: FighterDetails) {
    const { name } = fighter;
    const nameElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    nameElement.innerText = `Name: ${name}`;

    return nameElement;
}
function createFighterHealth(fighter: FighterDetails) {
    const { health } = fighter;
    const healthElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    healthElement.innerText = `Health: ${health}`;

    return healthElement;
}
function createFighterDefense(fighter: FighterDetails) {
    const { defense } = fighter;
    const defenseElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    defenseElement.innerText = `Defense: ${defense}`;

    return defenseElement;
}
function createFighterAttack(fighter: FighterDetails) {
    const { attack } = fighter;
    const attackElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-preview___info-el' });
    attackElement.innerText = `Attack: ${attack}`;

    return attackElement;
}

export function createFighterImage(fighter: Fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name,
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}